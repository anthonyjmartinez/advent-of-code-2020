use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::time::Instant;

fn part1(recs: &HashMap<String, HashMap<String, usize>>) -> usize {
    let mut keys: Vec<String> = Vec::new();
    p1_loop("shiny gold", &mut keys, recs);
    keys.sort();
    keys.dedup();
    keys.len()
}

fn p1_loop(key: &str,
	   keys: &mut Vec<String>,
	   recs: &HashMap<String, HashMap<String, usize>>) {
    for (bag_type, rules) in recs.iter() {
	if rules.contains_key(key) {
	    keys.push(bag_type.clone());
	    p1_loop(&bag_type, keys, recs);
	}
    }
}

fn part2(key: &str, recs: &HashMap<String, HashMap<String, usize>>) -> usize {
    let mut val: usize = 0;
    if let Some(rule) = recs.get(key) {
	for (key, qty) in rule.iter() {
	    val += *qty + *qty * part2(key, recs);
	}
    }
    val
}

fn parse_input<T: BufRead> (buf: T) -> HashMap<String, HashMap<String, usize>>{
    let mut data: HashMap<String, HashMap<String, usize>> = HashMap::new();
    for i in buf.lines() {
	if let Ok(line) = i {
	    let mut elements: Vec<&str> = line
		.split_ascii_whitespace()
		.filter(|x| ! x.contains("bag"))
		.filter(|x| ! x.contains("contain"))
		.collect();
	    let bag_type_elements: Vec<&str> = elements.drain(..2).collect();
	    let bag_type = format!("{} {}", bag_type_elements[0], bag_type_elements[1]);
	    let mut rules: HashMap<String, usize> = HashMap::new();
	    let mut element_count = elements.len();
	    while element_count > 0 {
		if element_count % 3 == 0 {
		    let data: Vec<_> = elements.drain(..3).collect();
		    let rule = format!("{} {}", data[1], data[2]);
		    rules.insert(rule, data[0].parse().unwrap());
		    element_count -= 3;
		} else {
		    let data: Vec<_> = elements.drain(..2).collect();
		    let rule = format!("{} {}", data[0], data[1]);
		    rules.insert(rule, 0);
		    element_count = 0;
		}
	    }
	    data.insert(bag_type, rules);
	}
    }
    data
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
	let input_file = File::open(&args[1])?;
	let f = BufReader::new(input_file);

	let now = Instant::now();
	let parsed = parse_input(f);
	println!("Parsing took {}μs", now.elapsed().as_micros());

	let now = Instant::now();
	let p1: usize = part1(&parsed);
	println!("Part1 took {}μs", now.elapsed().as_micros());
	println!("Answer to Part1: {}", p1);

	let now = Instant::now();
	let p2: usize = part2("shiny gold", &parsed);
	println!("Part2 took {}μs", now.elapsed().as_micros());
	println!("Answer to Part2: {}", p2);
    } else {
	panic!("Wrong number of args")
    }
    Ok(())
}
