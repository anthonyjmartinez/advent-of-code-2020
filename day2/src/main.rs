use std::env;
use std::fs;


struct PasswordEntry<'a> {
    upper: usize,
    lower: usize,
    rule: &'a str,
    pwd: &'a str,
}

impl<'a> PasswordEntry<'a> {
    fn validate_1(&self) -> bool {
	let found: Vec<&str> = self.pwd.matches(self.rule).collect();
	let found_len  = found.len();
	if self.lower <= found_len && self.upper >= found_len {
	    true
	} else {
	    false
	}
    }

    fn validate_2(&self) -> bool {
	let found: Vec<_> = self.pwd.match_indices(self.rule).map(|x| x.0).collect();
	let first: bool = found.contains(&(self.lower - 1));
	let second: bool = found.contains(&(self.upper - 1));
	if first && second {
	    false
	} else if first || second {
	    true
	} else {
	    false
	}
    }
}

fn parse_line(x: &str) -> PasswordEntry {
    let mut parts: Vec<&str> = x.split(" ").collect();
    let pwd: &str = parts.pop().unwrap();
    let rule: &str = parts.pop().unwrap().strip_suffix(":").unwrap();
    let mut bounds: Vec<usize> = parts.pop().unwrap().split("-").collect::<Vec<&str>>().iter().map(|x| x.parse().unwrap()).collect();
    let upper: usize = bounds.pop().unwrap();
    let lower: usize = bounds.pop().unwrap();

    PasswordEntry {
	upper,
	lower,
	rule,
	pwd,
    }
}


fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
	let data = fs::read_to_string(&args[1])?;
	let mut valid_1: u32 = 0;
	let mut valid_2: u32 = 0;
	for line in data.lines() {
	    let entry: PasswordEntry = parse_line(&line); 
	    if entry.validate_1() {
		valid_1 += 1;
	    }
	    if entry.validate_2() {
		valid_2 += 1;
	    }
	}
	println!("There are {} valid passwords (part 1)", valid_1);
	println!("There are {} valid passwords (part 2)", valid_2);
    } else {
	panic!("Only one argument please.")
    }
    Ok(())
}
