use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::time::Instant;

fn part1(data: &Vec<usize>) -> usize {
    let mut one: usize = 0;
    let mut three: usize = 0; 
    for (idx, val) in data.iter().enumerate() {
	if idx > 0 {
	    match val - data[idx-1] {
		1 => {
		    one += 1;
		},
		3 => {
		    three += 1;
		},
		_ => ()
	    }
	}
    }
    one * three
}

fn part2(data: Vec<usize>) -> usize {
    // Named closure to get the values from which a given value can be reached
    let possible = |d: &Vec<usize>, r: &&usize, i:&usize| -> Vec<_> {
	let mut low: usize = 0;
	if **r > 2 {
	    low = **r - 3;
	}

	// Set a lower limit for the scanning index
	let mut i_low: usize = 0;
	if *i > 3 {
	    i_low = *i - 3;
	}
	let vals: Vec<_> = (low..**r).filter(|x| d[i_low..*i].contains(x)).collect();
	vals
    };

    // HashMap to store running count of possible paths
    let mut map: HashMap<usize, usize> = HashMap::new();
    for (idx, rating) in data.iter().enumerate() {
	let p = possible(&data, &rating, &idx);
	if idx == 0 {
	    // Only one path from 0
	    map.insert(*rating, 1);
	} else {
	    let prev: usize = data[idx-1];
	    let mut prev_paths: usize = 0;
	    // Rolling diff showed there are only differences of 1 and 3, never 2.
	    match rating - prev {
		1 => {
		    // Loop across the possible values to
		    // accumulate all paths in range
		    for m in p {
			prev_paths += map.get(&m).unwrap();
			map.insert(*rating, prev_paths);
		    }
		},
		3 => {
		    // There's only ever one way to get over a gap of three
		    // thus a jump of three preserves the number of paths to
		    // the previous node, and adds nothing new itself.
		    map.insert(*rating, *map.get(&prev).unwrap());
		},
		_ => ()
	    }
	}
    }

    *map.get(&data
	     .iter()
	     .max()
	     .unwrap())
	.unwrap()
    
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
	let input_file = File::open(&args[1])?;
	let f = BufReader::new(input_file);
	let mut vals: Vec<usize> = f.lines().map(|x| x.unwrap().parse().unwrap()).collect();
	// Add the wall outlet and end device
	vals.push(0);
	vals.sort();
	vals.push(vals[vals.len() - 1] + 3);

	let now = Instant::now();
	let p1 = part1(&vals);
	println!("Part 1 took {}µs", now.elapsed().as_micros());
	println!("Part 1 answer is {}", p1);

	let now = Instant::now();
	let p2 = part2(vals);
	println!("Part 2 took {}µs", now.elapsed().as_micros());
	println!("Part 2 answer is {}", p2);
	
    }
    Ok(())
}
