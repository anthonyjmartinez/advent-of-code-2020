use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::time::Instant;


fn part1(data: &Vec<String>) -> isize {
    let mut acc: isize = 0;
    let mut pos: usize = 0;
    let elements: usize = data.len();
    let mut inst_set = data.clone();
    engine(&mut pos, &mut acc, &mut inst_set, &elements);
    acc
}

fn part2(data: &Vec<String>) -> isize {
    let mut p2_ans: isize = 0;
    let elements: usize = data.len();
    for i in 0..elements {
	let mut acc: isize = 0;
	let mut pos: usize = 0;
	let mut inst_set = data.clone();
	let val = &data[i];
	if val.contains("nop") {
	    inst_set[i] = inst_set[i].replace("nop", "jmp");
	    engine(&mut pos, &mut acc, &mut inst_set, &elements);
	    if pos == elements {
		p2_ans = acc;
		println!("Replaced value at idx: {}", i);
		break
	    }
	} else if val.contains("jmp") {
	    inst_set[i] = inst_set[i].replace("jmp", "nop");
	    engine(&mut pos, &mut acc, &mut inst_set, &elements);
	    if pos == elements {
		p2_ans = acc;
		println!("Replaced value at idx: {}", i);
		break
	    }
	} else {
	    continue
	}
    }
    p2_ans
}

fn engine(pos: &mut usize,
	  acc: &mut isize,
	  inst_set: &mut Vec<String>,
	  len: &usize) {
    loop {
	if *pos < *len {
	    let inst = inst_set[*pos].clone();
	    let op: Vec<&str> = inst.split_ascii_whitespace().collect();
	    if op.is_empty() {
		break
	    }
	    match op[0] {
		"nop" => {
		    inst_set[*pos] = String::new();
		    *pos += 1;
		    engine(pos, acc, inst_set, len);
		},
		"acc" => {
		    inst_set[*pos] = String::new();
		    *pos += 1;
		    *acc += op[1].parse::<isize>().unwrap();
		    engine(pos, acc, inst_set, len);
		},
		"jmp" => {
		    inst_set[*pos] = String::new();
		    let jump: isize = op[1].parse().unwrap();
		    if jump.is_negative() {
			*pos -= jump.abs() as usize;
		    } else {
			*pos += jump as usize;
		    }
		    engine(pos, acc, inst_set, len);
		},
		_ => ()
	    }
	} else {
	    break;
	}
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
	let input_file = File::open(&args[1])?;
	let f = BufReader::new(input_file);

	let now = Instant::now();
	let data: Vec<String> = f.lines().map(|s| s.unwrap()).collect();
	println!("Parsing took {}μs", now.elapsed().as_micros());

	let now = Instant::now();
	let p1: isize = part1(&data);
	println!("Part 1 took {}μs", now.elapsed().as_micros());
	println!("Part 1 answer is {}", p1);

	let now = Instant::now();
	let p2: isize = part2(&data);
	println!("Part 2 took {}μs", now.elapsed().as_micros());
	println!("Part 2 answer is {}", p2);
    } else {
	panic!("Wrong number of args")
    }
    Ok(())
}
