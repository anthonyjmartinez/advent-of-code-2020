use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn line_value(line: &str) -> usize {
    let binary_string = line.replace("B", "1")
	.replace("F", "0")
	.replace("L", "0")
	.replace("R", "1");
    usize::from_str_radix(&binary_string, 2).unwrap()
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
	let input_file = File::open(&args[1])?;
	let f = BufReader::new(input_file);
	let mut max: usize = 0;
	let mut min: usize = usize::MAX;
	let mut values: Vec<usize> = Vec::new();
	for line in f.lines() {
	    if let Ok(data) = line {
		let val = line_value(&data);
		values.push(val);
		if val > max {
		    max = val;
		} else if val < min {
		    min = val;
		}
	    }
	}
	println!("{} is the max seat value for part1", max);
	values.sort();
	let expected_values: Vec<usize> = (min..max).collect();
	for expected in expected_values.iter() {
	    if ! values.contains(expected) {
		println!("{} is my seat", expected);
	    }
	}
	Ok(())
    } else {
	panic!("Wrong number of args")
    }
}
