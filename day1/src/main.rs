use std::env;
use std::fs;


fn load_input(f: &str) -> Result<Vec<u32>, Box<dyn std::error::Error>> {
    let data_in = fs::read_to_string(f)?;
    Ok(data_in
       .lines()
       .filter_map(|x| x.parse::<u32>().ok())
       .collect::<Vec<u32>>())
}

fn part1(d: &Vec<u32>, tgt: &u32) {
    let mut found = false;
    for i in d.iter() {
	if found { break }
	for j in d.iter() {
	    if i + j == *tgt {
		println!("{} + {} = 2020", i, j);
		println!("Part 1 Answer: {}", i*j);
		found = true;
	    }
	}
    }
}

fn part2(d: &Vec<u32>, tgt: &u32) {
    let mut found = false;
    for i in d.iter() {
	if found { break }
	for j in d.iter() {
	    if found { break }
	    for k in d.iter() {
		if i + j + k == *tgt {
		    println!("{} + {} + {} = 2020", i, j, k);
		    println!("Part 2 Answer: {}", i*j*k);
		    found=true;
		}
	    }
	}
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    const TARGET: u32 = 2020;
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
	let input_path = &args[1];
	let mut data = load_input(&input_path)?;
	data.sort_unstable();
	part1(&data, &TARGET);
	part2(&data, &TARGET);
    } else {
	panic!("Only one argument please")
    }
    Ok(())
}
