# Advent of Code - 2020
## Rust

My attempts at the 2020 Advent of Code in Rust. My solutions will almost certainly not be the most
efficient in terms of cycles or memory use. Advice on how a given solution could be improved are
of course welcome. Keep in mind my background is not software, but Mechanical Engineering. While
I can tell you exactly why a particular machining tolerance or weldment should be called out for a
mechanical design, I have no idea what the big-O notation of any particular solution is or could be.

These attempts are in Rust because I find the best way to gain proficiency in anything is to do that
thing a lot.

