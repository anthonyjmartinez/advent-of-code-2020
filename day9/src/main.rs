use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::time::Instant;

fn part1(data: &Vec<isize>, pa_size: &usize) -> usize {
    let mut pos: usize = *pa_size;
    let mut ans: usize = 0;
    for val in &data[pos..] {
	let preamble = &data[pos - *pa_size..pos];
	let mut valid = false;
	for p in preamble {
	    let x = val - p;
	    if preamble.contains(&x) {
		valid = true;
		break
	    }
	}
	if valid {
	    pos += 1;
	} else {
	    ans = *val as usize;
	    break
	}
    }
    ans
}

fn part2(data: &Vec<isize>, inv: &usize) -> usize {
    let mut first: usize = 0;
    let mut last: usize = 1;
    let mut found = false;
    for _x in &data[first..] {
	for _y in &data[last..] {
	    let sum: isize = data[first..=last].iter().sum();
	    if sum as usize == *inv {
		found = true;
		break
	    } else {
		last += 1;
	    }
	}
	if found {
	    break
	} else {
	    first += 1;
	    last = first + 1;
	}
    }
    let mut win: Vec<&isize> = data.iter().skip(first).take(last-first).collect();
    win.sort();
    (*win.first().unwrap() + *win.last().unwrap()) as usize
}


fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 3 {
	let input_file = File::open(&args[1])?;
	let pa_size: usize = args[2].parse()?;
	let f = BufReader::new(input_file);
	let vals: Vec<isize> = f.lines().map(|x| x.unwrap().parse().unwrap()).collect();
	let now = Instant::now();
	let p1 = part1(&vals, &pa_size);
	println!("Part 1 took {}μs", now.elapsed().as_micros());
	println!("Part 1 answer is {}", p1);

	let now = Instant::now();
	let p2 = part2(&vals, &p1);
	println!("Part 2 took {}μs", now.elapsed().as_micros());
	println!("Part 2 answer is {}", p2);
    } else {
	panic!("Wrong number of args")
    }
    Ok(())
}
