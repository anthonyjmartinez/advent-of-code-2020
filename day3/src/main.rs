use std::fs;
use std::env;

/* x steps into the string, y steps into the vector */
fn run_map(tmap: &Vec<String>, x: usize, y: usize) -> usize {
    let mut right_pos: usize = 0;
    let mut trees: usize = 0;
    for (idx, line) in tmap.iter().enumerate() {
	if idx % y == 0 && idx != 0 {
	    let step = line.chars().nth(right_pos + x).unwrap().to_string();
	    if step == "#".to_string() {
		trees += 1;
	    }
	    right_pos += x;
	}
    }
    println!("{} trees on slope: {},{}", trees, x, y);
    trees
}

fn build_map(src: &String) -> Option<Vec<String>> {
    let map_base: Vec<&str> = src.lines().collect();
    let map_len = map_base.len();
    let tree_map = map_base.iter().map(|x| x.repeat(map_len)).collect();
    Some(tree_map)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
	let data = fs::read_to_string(&args[1])?;
	if let Some(tree_map) = build_map(&data) {
	    let tree_hits_2 = run_map(&tree_map, 1, 1);
	    let tree_hits_1 = run_map(&tree_map, 3, 1);
	    let tree_hits_3 = run_map(&tree_map, 5, 1);
	    let tree_hits_4 = run_map(&tree_map, 7, 1);
	    let tree_hits_5 = run_map(&tree_map, 1, 2);
	    println!("{} trees were hit. Part 1", tree_hits_1);
	    println!("{} trees hit in all. Part 2", tree_hits_1 * tree_hits_2 * tree_hits_3 * tree_hits_4 * tree_hits_5)
	}
    } else {
	panic!("please");
    }
    Ok(())
}
