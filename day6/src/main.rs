use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::time::Instant;

/**
Initial Part 1 solution using for loops and vector
operations to tally the number of questions with
a yes answer. The innermost loop is likely the
slowest given it inspects the entire existing vector
to check for the absence of the current character
before pushing it if not already present.

On my Intel(R) Core(TM) i5-7260U this solution
takes 213μs when run as a release binary
*/
fn part1(recs: &Vec<Vec<String>>) -> usize {
    let mut val: usize = 0;
    let mut rec_vec: Vec<char> = Vec::new();
    for rec in recs.iter() {
	for entry in rec.iter() {
	    for c in entry.chars() {
		if ! rec_vec.contains(&c) {
		    rec_vec.push(c)
		}
	    }
	}
	val += rec_vec.len();
	rec_vec.clear();
    }
    val
}

/**
A second solution to Part 1 using binary operations
on usize integers stored in a vector. This was developed
after using the technique on Part 2 where the solution
differs by exactly one character (| vs &).

On my Intel(R) Core(TM) i5-7260U this solution
takes 63μs when run as a release binary
*/
fn part1_bitwise(recs: &Vec<Vec<String>>) -> usize {
    let mut val_total: usize = 0;
    for rec in recs.iter() {
	let mut val_vec: Vec<usize> = Vec::new();
	for entry in rec.iter() {
	    let mut entry_val: usize = 0;
	    for c in entry.as_bytes().iter() {
		entry_val = entry_val | ( 1 << c - 97 );
	    }
	    val_vec.push(entry_val);
	}
	if let Some(mut rec_val) = val_vec.pop() {
	    for val in val_vec.iter() {
		rec_val = rec_val | val;
	    }
	    val_total += rec_val.count_ones() as usize;
	}
    }
    val_total
}

fn part2(recs: Vec<Vec<String>>) -> usize {
    let mut val_total: usize = 0;
    for rec in recs.iter() {
	let mut val_vec: Vec<usize> = Vec::new();
	for entry in rec.iter() {
	    let mut entry_val: usize = 0;
	    for c in entry.as_bytes().iter() {
		entry_val = entry_val | ( 1 << c - 97 );
	    }
	    val_vec.push(entry_val);
	}
	if let Some(mut rec_val) = val_vec.pop() {
	    for val in val_vec.iter() {
		rec_val = rec_val & val;
	    }
	    val_total += rec_val.count_ones() as usize;
	}
    }
    val_total
}

fn parse_input<T: BufRead> (buf: T) -> Vec<Vec<String>> {
    let mut records: Vec<Vec<String>> = Vec::new();
    let mut record: Vec<String> = Vec::new();
    for i in buf.lines() {
	if let Ok(line) = i {
	    if ! line.is_empty() {
		record.push(line);
	    } else {
		records.push(record.clone());
		record.clear();
	    }
	}
    }
    records.push(record);
    records
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
	let input_file = File::open(&args[1])?;
	let f = BufReader::new(input_file);
	let now = Instant::now();
	let parsed: Vec<Vec<String>> = parse_input(f);
	println!("File parsing took {}μs", now.elapsed().as_micros());

	let now = Instant::now();
	let p1: usize = part1(&parsed);
	println!("Part1 took {}μs", now.elapsed().as_micros());
	println!("{} is the answer to part1", p1);
	
	let now = Instant::now();
	let p1_bitwise: usize = part1_bitwise(&parsed);
	println!("Part1 (bitwise) took {}μs", now.elapsed().as_micros());
	println!("{} is the answer to part1", p1_bitwise);

	let now = Instant::now();
	let p2: usize = part2(parsed);
	println!("Part2 took {}μs", now.elapsed().as_micros());
	println!("{} is the answer to part2", p2);
    } else {
	panic!("Wrong number of args")
    }
    Ok(())
}
