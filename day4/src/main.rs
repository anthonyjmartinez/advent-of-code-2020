use std::collections::HashMap;
use std::env;
use std::fs;
use std::io::{BufRead, BufReader};

fn part1(recs: &Vec<HashMap<&str, &str>>, required: &Vec<&str>) -> (usize, usize) {
    let mut valid: usize = 0;
    let mut invalid: usize = 0;
    for rec in recs.iter() {
	let mut missing: usize = 0;
	for req in required.iter() {
	    if req != &"cid" && ! rec.contains_key(req) {
		missing += 1;
	    }
	}

	if missing == 0 {
	    valid += 1
	} else {
	    invalid += 1
	}
    }
   (valid, invalid)
}

fn part2(recs: &Vec<HashMap<&str, &str>>, required: &Vec<&str>) -> (usize,usize) {
    let check_yr = |k: &str, min: usize, max: usize, m: &HashMap<&str, &str>| -> bool {
	let mut s: bool = true;
	if let Ok(year) = m.get(k).unwrap().parse::<usize>() {
	    if year < min || year > max {
		s = false;
	    } else {
		s = true;
	    }
	}
	s
    };
    let mut valid: usize = 0;
    let mut invalid: usize = 0;
    let eye_colors = vec!("amb", "blu", "brn", "gry", "grn", "hzl", "oth");
    for rec in recs.iter() {
	let mut missing: usize = 0;
	let mut bad: usize = 0;
	for req in required.iter() {
	    if req != &"cid" && ! rec.contains_key(req) {
		missing += 1;
	    } else {
		match req {
		    &"byr" => {
			if ! check_yr(req, 1920, 2002, &rec) {
			    bad += 1;
			}
		    },
		    &"iyr" => {
			if ! check_yr(req, 2010, 2020, &rec) {
			    bad += 1;
			}
		    },
		    &"eyr" => {
			if ! check_yr(req, 2020, 2030, &rec) {
			    bad += 1;
			}
		    },
		    &"hgt" => {
			if let Some(hgt) = rec.get(req) {
			    let len = hgt.len();
			    let unit: &str = &hgt[len-2..];
			    if let Ok(val) = &hgt[..len-2].parse::<usize>() {
				if unit == "cm" && (*val < 150 || *val > 193){
				    bad += 1;
				}
				if unit == "in" && (*val < 59 || *val > 76) {
				    bad += 1;
				}
			    } else {
				bad += 1;
			    }
			}
		    },
		    &"hcl" => {
			if let Some(value) = rec.get(req) {
			    if value.starts_with("#") && value.len() == 7 {
				for i in value[1..].chars() {
				    if ! i.is_ascii_hexdigit() {
					bad += 1;
				    }
				}
			    } else {
				bad += 1;
			    }
			}
		    },
		    &"ecl" => {
			if let Some(val) = rec.get(req) {
			    if ! eye_colors.contains(val) {
				bad += 1;
			    }
			}
		    },
		    &"pid" => {
			if let Some(val) = rec.get(req) {
			    if let Ok(_) = val.parse::<usize>() {
				if val.len() != 9 {
				    bad += 1;
				}
			    } else {
				bad += 1;
			    }
			}
		    },
		    &"cid" => (),
		    _ => (),
		}
	    }
	}

	if missing == 0 && bad == 0 {
	    valid += 1
	} else {
	    invalid += 1
	}
    }
    (valid, invalid)
}

fn create_records<'a> (recs: &'a Vec<Vec<String>>) -> Vec<HashMap<&'a str, &'a str>>{
    let mut records: Vec<HashMap<&str, &str>> = Vec::new();
    for rec in recs.iter() {
	let mut rec_map: HashMap<&str, &str> = HashMap::new();
	for entry in rec.iter() {
	    let fields = entry.split(" ").collect::<Vec<&str>>();
	    for field in fields.iter() {
		let k = &field[..3]; //Keys are the first 3 elements
		let v = &field[4..]; //Values skip the fourth element (idx 3, a :)
		rec_map.insert(k, v);
	    }
	}
	records.push(rec_map);
    }
    records
}

fn parse_input<T: BufRead> (buf: T) -> Vec<Vec<String>> {
    let mut records: Vec<Vec<String>> = Vec::new();
    let mut record: Vec<String> = Vec::new();
    for i in buf.lines() {
	if let Ok(line) = i {
	    if ! line.is_empty() {
		record.push(line);
	    } else {
		records.push(record.clone());
		record.clear();
	    }
	}
    }
    records.push(record);
    records
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
	let in_file = fs::File::open(&args[1])?;
	let f = BufReader::new(in_file);
	let parsed: Vec<Vec<String>> = parse_input(f);
	let records: Vec<HashMap<&str, &str>> = create_records(&parsed);
	let required = vec!("ecl", "pid", "eyr", "hcl", "byr", "iyr", "hgt");
	println!("There are {} records", records.len());
	let (valid, invalid) = part1(&records, &required);
	println!("There are {} valid and {} invalid records for part1", valid, invalid);
	let (valid, invalid) = part2(&records, &required);
	println!("There are {} valid and {} invalid records for part2", valid, invalid);
    } else {
	panic!("Wrong number of args")
    }
   Ok(()) 
}
